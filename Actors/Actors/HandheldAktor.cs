﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Actors.Actors.HandHeldMessages;
using Akka.Actor;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WSMini;
using WSReceiptMemory;
using AuthSoapHeader = WSMini.AuthSoapHeader;
using Receipts_LineDetail = WSMini.Receipts_LineDetail;

namespace Actors.Actors
{
    public class HandheldAktor : ReceiveActor
    {
        private readonly ILogger<HandheldAktor> _logger;
        private readonly IMapper _mapper;
        private readonly IConfigurationRoot _configurationRoot;

        private WSMiniSoapClient _miniSoapClient;
        private WSReceiptMemorySoapClient _receiptMemorySoapClient;

        private int _currentTable;
        private int _cashregisterId;
        private WSMini.Receipt _receipt = null;
        private bool _tableOpen = false;
        private readonly Stopwatch _stopwatch;
        private bool _isRunning = false;

        public HandheldAktor(ILogger<HandheldAktor> logger, IMapper mapper, IConfigurationRoot configurationRoot)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _configurationRoot = configurationRoot ?? throw new ArgumentNullException(nameof(configurationRoot));

            _logger.LogDebug($"Handheld setup messages");
            Receive<Initialize>(message => ReceiveInitialize(message));
            Receive<SignOnHandheld>(message => ReceiveSignOnHandeld(message));

            _stopwatch = new Stopwatch();
        }

        private void ReceiveSignOnHandeld(SignOnHandheld message)
        {
            _miniSoapClient = new WSMiniSoapClient(WSMiniSoapClient.EndpointConfiguration.WSMiniSoap, message.WebserviceMiniUrl + "/WSmini.asmx");
            _receiptMemorySoapClient = new WSReceiptMemorySoapClient(WSReceiptMemorySoapClient.EndpointConfiguration.WSReceiptMemorySoap, message.WebserviceMiniUrl + "/WSReceiptMemory.asmx");

            _logger.LogDebug($"Trying to signon handheld {message.CashregisterId} on {message.WebserviceMiniUrl}");
            _cashregisterId = message.CashregisterId;

            _stopwatch.Start();
            var signInResult = _miniSoapClient.SignOnCashregisterAsync(new AuthSoapHeader(), message.CashregisterId);
            var response = signInResult.Result;

            _logger.LogDebug($"Handheld {message.CashregisterId} successfully signed on at {response.serverDateTime}");
            _logger.LogInformation($"SignOnCashregister time in millisecond {_stopwatch.ElapsedMilliseconds}");
            _stopwatch.Stop();
            _stopwatch.Reset();

            _receipt = new WSMini.Receipt();
            _receipt.CashRegisterId = _cashregisterId;
            _receipt.StoreId = 2;
            _receipt.CreationDate = DateTime.Now;



            //Task.FromResult(OpenTable());


            // Create a timer
            var myTimer = new System.Timers.Timer();
            // Tell the timer what to do when it elapses
            myTimer.Elapsed += (sender, args) => Task.FromResult(OpenTable());
            // Set it to go off every five seconds
            myTimer.Interval = 5000;
            myTimer.Enabled = true;


        }

        private void ProcessArticleDiscountRules()
        {
            AddArticleToReceipt();

            _logger.LogDebug($"Handheld {_cashregisterId } table {_currentTable} processArticleDiscountRules");
            _stopwatch.Start();

            var processDiscountRulesResult = _miniSoapClient.ProcessDiscountRulesOnReceiptAsync(new ProcessDiscountRulesOnReceiptRequest(new AuthSoapHeader(), _cashregisterId, _receipt, long.MinValue, 1)).Result;
            var result = processDiscountRulesResult.ProcessDiscountRulesOnReceiptResult;
            _logger.LogInformation($"ProcessDiscountRulesOnReceiptAsync time in millisecond {_stopwatch.ElapsedMilliseconds}");
            _stopwatch.Stop();
            _stopwatch.Reset();
        }

        private void AddArticleToReceipt()
        {

            var detail = new WSMini.Receipts_LineDetail();
            detail.AccessGroupId = long.MinValue;
            detail.CashregisterId = _cashregisterId;
            detail.ArticleStatus = "";
            detail.ItemId = 60;
            detail.ItemType = "PLU";
            detail.ArticleId = 60;
            detail.CommandLines = new WSMini.Receipts_LineCommand[0];
            detail.CourseId = long.MinValue;
            detail.DiscountRuleCombiDetailId = long.MinValue;
            detail.DiscountRuleDetailId = long.MinValue;
            detail.DiscountRuleIdManually = long.MinValue;
            detail.FixedPrice = false;
            detail.NetAmount = 0m;
            detail.NetAmountExcl = 0m;
            detail.Quantity = 1;
            detail.PersonelId = 1;
            detail.Description = "Begeleider invalidenabonnement";
            detail.DetailLineNumber = 0;
            detail.CreationDate = DateTime.Now;
            detail.GrossAmount = 0m;
            detail.GrossAmountExcl = 0m;
            detail.IsStoredInJournal = false;
            detail.MaingroupId = 1;
            detail.OrderPrintCount = 0;
            detail.PrinterGroupId = 2;
            detail.SubgroupId = 16;
            detail.GuidDL = Guid.NewGuid();
            detail.TaxId = 3;
            detail.PricelistId = long.MinValue;
            detail.TicketPrinted = false;
            detail.VisibleOnScreen = true;
            detail.NetPrice = false;
            detail.PaymentId = long.MinValue;
            detail.PersonelIdRegister = 1;
            detail.PrinterLocationId = long.MinValue;
            detail.ProfitcentreId = long.MinValue;
            detail.RMKindDetailId = _receipt.RMKindDetailId;
            detail.WellnessVisitorId = long.MinValue;
            detail.ZReportPersonelId = 1;

            var detail2 = new WSMini.Receipts_LineDetail();
            detail2.AccessGroupId = long.MinValue;
            detail2.CashregisterId = _cashregisterId;
            detail2.ArticleStatus = "";
            detail2.ItemId = 56;
            detail2.ItemType = "PLU";
            detail2.ArticleId = 56;
            detail2.CommandLines = new WSMini.Receipts_LineCommand[0];
            detail2.CourseId = long.MinValue;
            detail2.DiscountRuleCombiDetailId = long.MinValue;
            detail2.DiscountRuleDetailId = long.MinValue;
            detail2.DiscountRuleIdManually = long.MinValue;
            detail2.FixedPrice = false;
            detail2.NetAmount = 0m;
            detail2.NetAmountExcl = 0m;
            detail2.Quantity = 1;
            detail2.PersonelId = 1;
            detail2.Description = "Entree 55+, woe na 14.00 uur";
            detail2.DetailLineNumber = 0;
            detail2.CreationDate = DateTime.Now;
            detail2.GrossAmount = 0m;
            detail2.GrossAmountExcl = 0m;
            detail2.IsStoredInJournal = false;
            detail2.MaingroupId = 1;
            detail2.OrderPrintCount = 0;
            detail2.PrinterGroupId = 2;
            detail2.SubgroupId = 6;
            detail2.GuidDL = Guid.NewGuid();
            detail2.TaxId = 3;
            detail2.PricelistId = long.MinValue;
            detail2.TicketPrinted = false;
            detail2.VisibleOnScreen = true;
            detail2.NetPrice = false;
            detail2.PaymentId = long.MinValue;
            detail2.PersonelIdRegister = 1;
            detail2.PrinterLocationId = long.MinValue;
            detail2.ProfitcentreId = long.MinValue;
            detail2.RMKindDetailId = _receipt.RMKindDetailId;
            detail2.WellnessVisitorId = long.MinValue;
            detail2.ZReportPersonelId = 1;

           

            var receiptLine = new WSMini.Receipts_Line();
            receiptLine.CashregisterId = _cashregisterId;
            receiptLine.ItemType = "PLU";
            receiptLine.ItemId = 60;
            receiptLine.Description = "Begeleider invalidenabonnement";
            receiptLine.PersonelId = 1;
            receiptLine.ProfitcentreId = long.MinValue;
            receiptLine.PricelistId = long.MinValue;
            receiptLine.PersonelIdRegister = 1;
            

            var receiptLine2 = new WSMini.Receipts_Line();
            receiptLine2.CashregisterId = _cashregisterId;
            receiptLine2.ItemType = "PLU";
            receiptLine2.ItemId = 56;
            receiptLine2.Description = "Entree 55+, woe na 14.00 uur";
            receiptLine2.PersonelId = 1;
            receiptLine2.ProfitcentreId = long.MinValue;
            receiptLine2.PricelistId = long.MinValue;
            receiptLine2.PersonelIdRegister = 1;

            

            receiptLine.DetailedLines = new WSMini.Receipts_LineDetail[1];
            receiptLine.DetailedLines[0] = detail;

            receiptLine2.DetailedLines = new Receipts_LineDetail[1];
            receiptLine2.DetailedLines[0] = detail2;


            _receipt.TableId = _currentTable;
            _receipt.TransactionDateTime = DateTime.Now;
            _receipt.CreationDate = DateTime.Now;
            _receipt.CustomerCount = 1;
            _receipt.StadiumEventId = long.MinValue;
            _receipt.RMSubNumber = int.MinValue;
            _receipt.PaymentId = long.MinValue;
            _receipt.WellnessVisitorId = long.MinValue;
            _receipt.RMSubNumber = 0;
            _receipt.PersonelIdFinish = long.MinValue;
            _receipt.PersonelKeyId = long.MinValue;
            _receipt.PersonelId = 1;
            _receipt.StoreId = 2;
            _receipt.ZReportPersonelId = 1;
            
            _receipt.Lines = new WSMini.Receipts_Line[2];
            _receipt.Lines[0] = receiptLine;
            _receipt.Lines[1] = receiptLine2;
          

        }

        private async Task OpenTable()
        {
            _logger.LogDebug($"Handheld {_cashregisterId} set isRunning to {_isRunning}");

            if (!_tableOpen && !_isRunning)
            {
                var randomGenerator = new Random();
                _currentTable = randomGenerator.Next(1, 8);

                _logger.LogDebug($"Handheld {_cashregisterId } try to open table {_currentTable}");

                var receiptMemoryKind = _receiptMemorySoapClient.GetRMKindAsync(new WSReceiptMemory.AuthSoapHeader(), 4).Result;

                if (receiptMemoryKind.GetRMKindResult != null)
                {
                    _stopwatch.Start();

                    var openResult = _receiptMemorySoapClient.OpenReceiptMemoryFullExclusiveAsync(
                        new OpenReceiptMemoryFullExclusiveRequest(new WSReceiptMemory.AuthSoapHeader(),
                            receiptMemoryKind.GetRMKindResult.RMKindId, _currentTable, 0, _cashregisterId, string.Empty)).Result;

                    _logger.LogInformation($"OpenReceiptMemoryFullExclusiveAsync time in millisecond {_stopwatch.ElapsedMilliseconds}");

                    _stopwatch.Stop();
                    _stopwatch.Reset();

                    if (openResult.rmLock.CashregisterId == _cashregisterId)
                    {
                        var receipt = openResult.OpenReceiptMemoryFullExclusiveResult;

                        TransformWsReceiptMemoryToWsMiniReceipt(receipt, openResult.rmKindDetail);

                        _logger.LogInformation($"Handheld {_cashregisterId } table {_currentTable} opened");

                        _tableOpen = true;

                        ProcessArticleDiscountRules();
                    }

                    _stopwatch.Start();

                    await CloseTable(openResult.rmLock);

                    _logger.LogInformation($"CloseTable time in millisecond {_stopwatch.ElapsedMilliseconds}");
                    _stopwatch.Stop();
                    _stopwatch.Reset();
                }
            }
        }

        private void TransformWsReceiptMemoryToWsMiniReceipt(WSReceiptMemory.Receipt memoryReceipt, RMKindDetail rmkindDetail)
        {
            var result = _mapper.Map<WSMini.Receipt>(memoryReceipt);
            result.RMKindDetailId = rmkindDetail.RMKindDetailId;
            result.TableId = rmkindDetail.Number;
            _receipt = result;
        }

        private async Task CloseTable(RMLock lockTable)
        {
            var random = new Random();
            var sleep = random.Next(0, 5);

            Thread.Sleep(sleep);

            _logger.LogDebug($"Handheld {_cashregisterId } try to close table {_currentTable}");

            if (_cashregisterId == int.MinValue)
                throw new ArgumentNullException(nameof(_cashregisterId));
            if (_currentTable == int.MinValue)
                throw new ArgumentNullException(nameof(_currentTable));

            var response = await _receiptMemorySoapClient.CloseReceiptMemoryAsync(new WSReceiptMemory.AuthSoapHeader(), lockTable.RMLockId);

            if (response != null)
            {
                _logger.LogDebug($"Handheld {_cashregisterId } table {_currentTable} closed");

                await FinishReceipt();

                Thread.Sleep(2000);
            }

            _logger.LogDebug($"Handheld {_cashregisterId} set isRunning to false");
            _isRunning = false;
        }

        private async Task FinishReceipt()
        {
            try
            {
                _logger.LogInformation($"Handheld {_cashregisterId} finishreceipt for table {_currentTable}");

                var finishReceipt = _miniSoapClient.FinishReceiptAsync(new FinishReceiptRequest(new AuthSoapHeader(),
                    Guid.NewGuid(), _cashregisterId, _receipt, long.MinValue, null, long.MinValue, null, long.MinValue, false,
                    false)).Result;

                if (!string.IsNullOrWhiteSpace(finishReceipt.ErrorMessage))
                {
                    _logger.LogError(finishReceipt.ErrorMessage);
                    throw new Exception(finishReceipt.ErrorMessage);
                }

                foreach (var msg in finishReceipt.InfoMessages)
                {
                    _logger.LogWarning(msg);
                }

                _logger.LogInformation($"Handheld {_cashregisterId} finishreceipt done");

                _tableOpen = false;
            }
            catch (Exception ex)
            {
                _tableOpen = true;
                _logger.LogError(ex.Message);
            }
        }

        private void ReceiveInitialize(Initialize message)
        {
            _logger.LogDebug($"Handheld initalizing");
        }
    }
}
