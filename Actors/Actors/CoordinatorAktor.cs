﻿using System;
using System.Collections.Generic;
using System.Text;
using Actors.Actors.CoordinatorMessages;
using Actors.Actors.HandHeldMessages;
using Akka.Actor;
using Akka.DI.Core;
using Akka.Event;
using Akka.Logger.Serilog;
using Microsoft.Extensions.Logging;

namespace Actors.Actors
{
    public class CoordinatorAktor : ReceiveActor
    {
        private readonly ILogger<CoordinatorAktor> _logger; //= Context.GetLogger<SerilogLoggingAdapter>();

        public CoordinatorAktor(ILogger<CoordinatorAktor> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _logger.LogInformation($"Coordinator setup messages");
            Receive<Start>(message => StartingUp(message));
        }

        private void StartingUp(Start startMessage)
        {

            foreach (var handheldNumber in startMessage.CashregisterIds)
            {
                _logger.LogInformation($"Startingup Handheld {handheldNumber} for url {startMessage.UrlWebservice}");
                
                var procesAktor = Context.ActorOf(Context.DI().Props<HandheldAktor>());
                procesAktor.Tell(new SignOnHandheld { CashregisterId = handheldNumber, WebserviceMiniUrl = startMessage.UrlWebservice });
            }

        }


    }
}
