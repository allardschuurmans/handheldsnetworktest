﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actors.Actors.HandHeldMessages
{
    public class SignOnHandheld
    {
        public int CashregisterId { get; set; }
        public string WebserviceMiniUrl { get; set; }
    }
}
