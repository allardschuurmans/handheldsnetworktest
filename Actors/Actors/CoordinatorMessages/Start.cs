﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actors.Actors.CoordinatorMessages
{
    public class Start
    {
        public List<int> CashregisterIds { get; set; }
        public string UrlWebservice { get; set; }
    }
}
