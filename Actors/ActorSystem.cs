﻿using System;
using System.Collections.Generic;
using Actors.Actors;
using Actors.Actors.CoordinatorMessages;
using Akka.Actor;
using Akka.DI.AutoFac;
using Autofac;

namespace Actors
{
    public class ActorSystem
    {
        private static Akka.Actor.ActorSystem _systemActor;
        private static IActorRef _coordinatorAktor;


        public static void Initialize(IContainer container)
        {
            if (_systemActor == null)
            {
                _systemActor = Akka.Actor.ActorSystem.Create(ActorNames.System); //"akka { loglevel=DEBUG, loggers=[\"Akka.Logger.Serilog.SerilogLogger, Akka.Logger.Serilog\"]}");
                var akkaDependencyResolver = new AutoFacDependencyResolver(container, _systemActor);
                _coordinatorAktor = _systemActor.ActorOf(akkaDependencyResolver.Create<CoordinatorAktor>(), ActorNames.Coordinator);
            }
        }

        public static bool StartUp(List<int> cashregisters, string url)
        {

            _coordinatorAktor.Tell(new Start { CashregisterIds = cashregisters, UrlWebservice = url });
            return true;
        }
    }
}
