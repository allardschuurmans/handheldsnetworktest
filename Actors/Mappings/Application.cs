﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;

namespace Actors.Mappings
{
    public class Application : Profile
    {
        public Application()
        {
            CreateMap<WSReceiptMemory.Receipt, WSMini.Receipt>();
        }
    }
}
