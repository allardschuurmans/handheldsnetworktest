﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actors
{
    public class ActorNames
    {
        public const string System = "System";
        public const string Coordinator = "Coordinator";
        public const string Handheld = "Handheld";
        public const string Proces = "Proces";
    }
}
