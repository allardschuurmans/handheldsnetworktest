﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HandHeldsFlowTests.Settings
{
    public class MainSettings
    {
        public int[] Handhelds { get; set; }
        public string WebserviceMiniUrl { get; set; }
    }
}
