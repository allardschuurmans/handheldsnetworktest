﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Actors;
using AutoMapper;
using AutoMapper.Configuration;
using HandHeldsFlowTests.Mappings;
using HandHeldsFlowTests.Services;

namespace HandHeldsFlowTests.Automapper
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x => x.AddProfiles(new []
            {
                typeof(ActorSystem),
                typeof(Orchestration)
            }));
        }
    }
}
