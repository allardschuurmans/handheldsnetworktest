﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;

namespace HandHeldsFlowTests.Interfaces
{
    public interface IOrchestration
    {
        void Run(IContainer container);
    }
}
