﻿using System;
using System.Configuration;
using System.IO;
using Actors.Actors;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using HandHeldsFlowTests.Automapper;
using HandHeldsFlowTests.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using WSMini;

namespace HandHeldsFlowTests
{
    class Program
    {
        private static IConfigurationRoot _configurationRoot;

        static void Main(string[] args)
        {
            _configurationRoot = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            SetupServiceProvider(serviceCollection);

            var builder = new ContainerBuilder();
            builder.Populate(serviceCollection);
            var applicationBuilder = builder.Build();

            AutoMapperConfiguration.Configure();

            //Go go gadget gas!
            applicationBuilder.Resolve<Orchestration>().Run(applicationBuilder);
            
            Console.ReadKey();
        }

        private static void SetupServiceProvider(ServiceCollection serviceCollection)
        {
            // Create service provider with Serilog
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var loggerFactory = serviceProvider.GetService<ILoggerFactory>();
            loggerFactory.AddSerilog();
            
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(_configurationRoot)
                .Enrich.FromLogContext()
                .CreateLogger();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            //Configure dependencies
            serviceCollection.AddLogging(seri => seri.AddSerilog());
            serviceCollection.AddSingleton<IConfigurationRoot>(_configurationRoot);
            serviceCollection.AddTransient<Orchestration>();
            serviceCollection.AddTransient<CoordinatorAktor>();
            serviceCollection.AddTransient<HandheldAktor>();
            serviceCollection.AddAutofac();
            serviceCollection.AddAutoMapper();

        }


    }
}
