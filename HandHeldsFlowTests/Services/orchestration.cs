﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Actors;
using Actors.Actors;
using Akka.Actor;
using Akka.DI.AutoFac;
using Akka.DI.Core;
using Autofac;
using HandHeldsFlowTests.Interfaces;
using HandHeldsFlowTests.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ActorSystem = Actors.ActorSystem;

namespace HandHeldsFlowTests.Services
{
    public class Orchestration : IOrchestration
    {
        private readonly ILogger<Orchestration> _logger;
        private readonly IConfigurationRoot _configuration;


        public Orchestration(ILogger<Orchestration> logger, IConfigurationRoot configuration)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public void Run(IContainer container)
        {
            _logger.LogInformation("Start initialisation of the actor system");
            ActorSystem.Initialize(container);
            Thread.Sleep(2000);

            var mainSettings = new MainSettings();
            _configuration.GetSection("MainSetting").Bind(mainSettings);

            ActorSystem.StartUp(mainSettings.Handhelds.ToList(), mainSettings.WebserviceMiniUrl);


        }
    }
}
